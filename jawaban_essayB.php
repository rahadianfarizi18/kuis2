<?php
    function perolehan_medali($array_medali){
        $result = [];
        if($array_medali == []){
            return "no data";
        }
        else{
            foreach($array_medali as $data){
                $get_key = NULL;
                foreach($result as $key => $value){
                    if($value['negara'] == $data[0]){
                        $get_key = $key;
                        break;
                    }
                }
                if($get_key !== NULL){
                    $result[$get_key][$data[1]] += 1;
                }
                else{
                   $result[] = array(
                       "negara" => $data[0],
                       "emas" => 0,
                       "perak" => 0,
                       "perunggu" => 0
                   );
    
                   $result[count($result)-1][$data[1]] += 1;
    
                }
            }
            return $result;
        }
    }

?>